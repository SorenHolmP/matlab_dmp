clc; clear;
close all;

%%
%Generate a trajectory:
dt = 0.01;
t0 = 0;
t_end = 10;
t= t0:dt:t_end;
theta = pi/6;

px0 = 0;
py0 = 0;
v = 100;
g = 9.82;

px = px0 + cos(theta) * 100 * t;                        %x(t)
py = py0 + sin(theta) * 100 * t - 0.5 * g * t.^2;       %y(t)

y_des = [px; py];
y0    = [y_des(1,1); y_des(2,1)];

%plot(px,py)


%%
close all;
az = 1;
bz = az / 4; %Critically damped system
ax = 1;
tau = 1;

%Canonical system:
x0 = 1;
x = x0 * exp(-ax * t * tau);

%desired velocity and acceleration:
yd_des = [];
ydd_des = [];
for i = 1:size(y_des,1) %For every dimension in the trajectory
    yd_des(i,:)     = [0 diff(y_des(i,:)) ./ dt];      %Unsure about whether to prepend initial velocity
    ydd_des(i,:)    = [0 diff(yd_des(i,:))./ dt];      %Or just a 0?
end

%Basis functions
num_bfs = 10;
t2 = linspace(0,t_end,num_bfs);  %linear spacing in time
mu = x0 * exp(-ax * t2);          %exponential spacing in x:
h   = num_bfs ./ (mu.^2);         % hi = #BFS / Ci^2 --> comment by Anne on Studywolf._.

bfs = [];

for i = 1:num_bfs
    bfs(i,:) = exp(-h(i) * (x-mu(i)).^2);
end


figure(1)
subplot(2,1,1)
for i = 1:num_bfs
    plot(t,bfs(i,:))
    hold on
end
xlabel('t')

subplot(2,1,2)
for i = 1:num_bfs
    plot(x,bfs(i,:))
    hold on
end
xlabel('x')


%Construct f_target

G = [y_des(1, length(y_des)); y_des(2, length(y_des))]; %goal position = last point in trajectory
F_target = ydd_des - az * (bz * (G - y_des) - yd_des); %Remember to mul. by tau... (or not?)

%Create s and gamma
n_dims = size(y_des,1);
S = [];
for i = 1:n_dims
    S(:,i) = x' * (G(i)-y0(i));
end

%Calculate weights for following x trajectory. Based on p. 344 in Ijspeert & SChaal
w = [];
s = S(:,1); %Doing the transpose part to get col. vector as p. 344 in Ijspeert & Schaal
f_target = F_target(1,:)';

for i = 1:num_bfs
    ri = diag(bfs(i,:));
    w(i) = s' * ri * f_target / (s' * ri * s);
end

%Calculate f (eq. 2.3) p. 333 Ijspeert & Schaal
num = sum( w' .* bfs ); % elementwise multiplication of weights onto bfs
den = sum( bfs ); 
f   = (num ./ den) .* x; % * (G(1) - y0(1));



%-----------Replicate x trajectory with found f-------------%
y_arr = zeros(1,length(t));
z_arr = zeros(1,length(t));

g = G(1); %Use endpoint of movement as goal
%g = 1200;  %Or define your own goal
%Initial conditions: (in doubt about z)
y = y0(1);
z = 0;
%tau = 1;

for i = 1:length(t)
    ydot = tau * z;
    zdot = tau * ( az * (bz * (g - y) - z) + f(i) * (g - y0(1)) );

    y = y + ydot * dt;
    z = z + zdot * dt;

    y_arr(i) = y;
    z_arr(i) = z;
end

figure(2)
subplot(3,1,1)

plot(t, y_arr, 'b--');
hold on
plot(t, y_des(1,:), 'r');
legend('Trained x trajectory', 'Desired x trajectory');


%------------ Do the same for y position ----------%
w = [];
s = S(:,2); %Doing the transpose part to get col. vector as p. 344 in Ijspeert & Schaal
f_target = F_target(2,:)';

for i = 1:num_bfs
    ri = diag(bfs(i,:));
    w(i) = s' * ri * f_target / (s' * ri * s);
end


num = sum( w' .* bfs ); % elementwise multiplication of weights onto bfs
den = sum( bfs ); 
f   = (num ./ den) .* x; %* (G(2) - y0(2));

y_arr2 = zeros(1,length(t));
z_arr2 = zeros(1,length(t));

g = G(2); %Use endpoint of movement as goal
%g = 100;  %Or define your own goal
%Initial conditions: (in doubt about z)
y = y0(2);
z = 0;
tau = 1;

for i = 1:length(t)
    ydot = z;
    zdot = az * (bz * (g - y) - z) + f(i) * (g - y0(2));

    y = y + ydot * dt;
    z = z + zdot * dt;

    y_arr2(i) = y;
    z_arr2(i) = z;
end

subplot(3,1,2)
plot(t, y_arr2, 'b--');
hold on
plot(t, y_des(2,:), 'r');
legend('Trained y trajectory', 'Desired y trajectory');



%Plot desired trajectory versus trained:
subplot(3,1,3)
plot(y_arr, y_arr2,'b--');
hold on
plot(y_des(1,:), y_des(2,:), 'r')
legend('Trained trajectory', 'Desired trajectory');



