classdef DMP < handle
    properties
        az 
        bz 
        ax 
        dt
        
        n_dims
        n_bfs
        
        y_des 
        yd_des
        ydd_des
        timestep 
        
        t_end    %The time when the desired trajectory reaches its goal
        tsim_end %The time the simulation stops
        time_vec %Array holding linear spaced timestamps
        P        %the index at the end of the desired trajectory this time
        
        
        %Canonical system:
        x           %x(t) the canonical system
        x0
        
        %Basis functions
        bfs         
        mu
        h
        
        %F_target
        F_target
        
        %Weights
        W
        
        %Forcing term:
        F

    end
    
    methods
        function this = DMP(az, bz, ax, dt, y_des, timestep, n_bfs)
            this.az = az;
            this.bz = bz;
            this.ax = ax;
            this.dt = dt;
            this.timestep = timestep;
            this.n_bfs = n_bfs
            
            
            this.n_dims = size(y_des,1);
            
            this.t_end = length(y_des) * timestep - timestep;
            this.tsim_end = 3 * this.t_end;
            this.time_vec = 0:dt:this.tsim_end;
            
            
            this.make_desired_traj(y_des);
            
            %this.make_canonical_system(1)
            %this.make_basis_functions()
            
            
            
        end
        
        function make_desired_traj(this, y_des)           
            for i=1:this.n_dims
                this.y_des(i,:) = interp1(0:this.timestep:this.t_end,y_des(i,:), 0:this.dt:this.t_end);
            end
            for i = 1:this.n_dims 
                this.yd_des(i,:)     = [0 diff(this.y_des(i,:)) ./ this.dt];      %Unsure about whether to prepend initial velocity
                this.ydd_des(i,:)    = [0 diff(this.yd_des(i,:))./ this.dt];      %Or just a 0?
            end
            
            this.P = length(this.y_des);
        end
        
        function make_canonical_system(this, x0)
            t = 0:this.dt:this.tsim_end;
            this.x0 = x0;
            this.x = x0 * exp(-this.ax * t);
            
            figure(1)
            plot(t,this.x,'linewidth',2);
            title('Canonical system x(t)');

            xlabel('t')
            
            
        end
        
        function make_basis_functions(this)
            t_eq        = linspace(0, this.t_end, this.n_bfs); %Centers equally spaced in time
            %t_eq        = linspace(0, this.tsim_end, this.n_bfs); %Centers equally spaced in time
            this.mu     = this.x0 * exp(-this.ax * t_eq);
            this.h      = this.n_bfs ./ (this.mu.^2);
            
            
            for i = 1:this.n_bfs
                this.bfs(i,:) = exp(-this.h(i) * (this.x - this.mu(i)).^2);
            end
                        
%             figure(2)
%             subplot(2,1,1)
%             for i = 1:this.n_bfs
%                 plot(this.time_vec,this.bfs(i,:))
%                 hold on
%             end
%             xlabel('t')
%             
%             subplot(2,1,2)
%             for i = 1:this.n_bfs
%                 plot(this.x,this.bfs(i,:))
%                 hold on
%             end
%             xlabel('x')
           
        end
        
        function find_weights(this)
            
            G = zeros(this.n_dims, length(this.y_des));
            for i=1:this.n_dims
                G(i,:) = this.y_des(i, length(this.y_des)); %Take final value of every trajectory as goal
            end
            
            %G = [y_des(1, length(y_des)); y_des(2, length(y_des))]
            F_target = this.ydd_des - this.az * (this.bz * (G - this.y_des) - this.yd_des);
            
            S = [];
            for i = 1:this.n_dims
                S(:,i) = this.x' * (G(i)-this.y_des(i,1));
            end
            
            
            P = length(this.y_des)
            for i = 1:this.n_dims
                s = S(1:P,i);
                f_target = F_target(i,:)';
                for j = 1:this.n_bfs
                   rj = diag(this.bfs(j,1:P));
                   wj = s' * rj * f_target / (s' * rj *s);
                   this.W(j,i) = wj;
                end
            end
            
            %Calculate F:
            for i = 1:this.n_dims
                num = sum (this.W(:,i) .* this.bfs )
                den = sum ( this.bfs )
                this.F(i,:) = (num ./ den) .* this.x;
            end
            
        end
        
        
        function rollout(this, G) %(should also be able to alter 'G, y0 and tau')
            
            
            if nargin == 1
                G = [];
                for i=1:this.n_dims
                    G(i,:) = this.y_des(i, length(this.y_des)) %Take final value of every trajectory as goal
                end
            end
            
            
            y0 = this.y_des(:,1);
            y = y0;
            z = zeros(this.n_dims, 1);
            
            y_arr = zeros(this.n_dims, length(this.time_vec));            
            z_arr = zeros(this.n_dims, length(this.time_vec));
            
            for i = 1:length(0:this.dt:this.t_end)%1:length(this.time_vec)
                ydot = z;
                zdot(:,1) = this.az * (this.bz * (G - y) - z) + this.F(:,i) .* (G - y0);
                
                y = y + ydot * this.dt;
                z = z + zdot * this.dt;
                
                y_arr(:,i) = y;
                z_arr(:,i) = z;
                
            end
           
            figure(3)
            subplot(3,1,1)
            plot(this.time_vec, y_arr(1,:),'b--','linewidth', 2);
            hold on 
            plot(0:this.dt:this.t_end, this.y_des(1,:),'r')
            
            legend('Trained trajectory dimension 1', 'Input trajectory')
            
            if(this.n_dims > 1) 
                subplot(3,1,2)
                plot(this.time_vec, y_arr(2,:),'b--','linewidth', 2);
                hold on
                plot(0:this.dt:this.t_end, this.y_des(2,:),'r')
                legend('Trained trajectory dimension 2', 'Input trajectory')


                subplot(3,1,3)
                plot(y_arr(1,:), y_arr(2,:),'b--')
                hold on
                plot(this.y_des(1,:), this.y_des(2,:), 'r');
                legend('Trained trajectory' , 'Input trajectory')
            end
            
            
        end
    end

end