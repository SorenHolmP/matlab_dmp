clc; clear;
close all;

% y_des = [1 5 10 17 19 20 10 50 80 110 120;
%          10 30 20 65 99 63 52 33 14 15 70                              ];
% timestep = 1;
% t_stop      = 10;



TG          = TrajectoryGenerator
timestep    = 0.01;
t_stop      = 1;
%y_des       = TG.step_function(t_stop,timestep)
y_des       = TG.projectile_motion(t_stop, timestep, pi/4, 10)

az = 1;
bz = az / 4;
ax = 1;
dt = 0.01;
num_bfs = 1000;


dmpObj = DMP(az, bz, ax, dt, y_des, timestep, num_bfs)

dmpObj.make_canonical_system(1)
dmpObj.make_basis_functions()
dmpObj.find_weights()
%G = 3; tau = 2;
dmpObj.rollout();