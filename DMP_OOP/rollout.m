%c: centers of basis functions
%h: variance of basis functions
function y_track = rollout(az, bz, ax, tau, dt, weights, c, h, g, y0, obstacles, runtime)
    n_dim = size(weights,2);
    
    function forcing_term = f(x)
        for d = 1:n_dim
            psi = exp(-h .* (x - c).^2);  
            num = dot(psi,weights(:,d));
            den = sum(psi);
            forcing_term(d) = (num / den) * x;
        end
    end

    %initial conditions:
    y = y0;
    z = zeros(n_dim,1);
    x = 1;
    
    t = 0;
    y_array =   [y0];
    x_array =   [x];
    z_array =   [z];
    f_array =   [f(x)];
    
%     rf_array    = repulsive_force_apf(y0', obstacles);
    
    
    %simulation:
    while t < runtime;
        ydot = tau * z;
%         zdot = tau * ( az * (bz * (g - y) - z) + f(x)' .* (g - y0) )
        zdot = tau * ( az * (bz * (g - y) - z) + f(x)' .* (g - y0) ) + repulsive_force_apf(y', obstacles)'; %avoid_obstacle(y,ydot,obstacles, g);
%         zdot = tau * ( az * (bz * (g - y) - z) + f(x)' .* (g - y0) ) + repulsive_force_avoid(y,ydot,obstacles, g);

        xdot = -ax * tau * x;
        
        y = y + ydot * dt;
        z = z + zdot * dt;
        x = x + xdot * dt;

        t = t + dt;
                
        y_array     = [y_array y];
        x_array     = [x_array x];
%         z_array     = [z_array z];
%         f_array     = [f_array; f(x)];
%         rf_array    = [rf_array; rf(y', obstacles)];

    end
    %plot(vecnorm(f_array,2,2),'LineWidth', 3)
%     hold on
%     plot(rf_array(:,1),'b','LineWidth', 1.5)
%     plot(rf_array(:,2),'r','LineWidth', 1.5)
%     plot(rf_array(:,1), rf_array(:,2));
%     plot(vecnorm(rf_array,2,2),'LineWidth', 1.5)
%     plot(z_array(1,:),'LineWidth', 1.5)
%     plot(z_array(2,:),'LineWidth', 1.5)
    
    %legend('norm forcing term', 'rf_{array} dim 1', 'rf_array dim 2', 'norm rf', 'z_array dim 1', 'z_array dim 2');
    

    y_track = y_array;

end