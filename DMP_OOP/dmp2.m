clc; clear; close all;

TG          = TrajectoryGenerator;
t_stop      = 1;
% timestep    = 0.01;        %Duration between each sample
% dt          = 0.01;
% theta       = pi/4;
% v0          = 10;
%y_des = TG.projectile_motion(t_stop, timestep, theta, v0);
%y_des  = TG.step_function(t_stop,timestep);
% y_demo = [1 4 3 2.5 4;
%           1 2 4 6   7
%           1 3 5 2   2];
%y_demo = [1 4 3 2.5 4 4 4 4;
%          1 2 4 6   7 7 7 7];
%y_des  = TG.splined_motion(y_demo, t_stop, dt);
% inigo_data  = importdata('trajectories/demo2.dat');
% inigo_data_ds = downsample(inigo_data(:,1:2),10);
% y_des = inigo_data_ds(1:801,:)'
[y_des, obstacles] = getUserTrajectory()
dt = t_stop / (length(y_des) - 1);
timestep = dt;

y_des = [y_des; linspace(0.20,0.21,201)];%Put in a trajectory for z-coordinate
% obstacles = [obstacles 0.2 * ones(size(obstacles,1),1)]
obstacles = getObstaclesFromTriangles()

az          = 50;
bz          = az / 4;
ax          = 1;
tau         = 1;
num_bfs     = 500;
g           = y_des(:,end);
y0          = y_des(:,1);
%obstacles   = [];%[2 1.05; 3.05 6.75]; %obstacles = [2.84 2.04];
simulation_runtime = t_stop * 1.5;            


[weights, centers, variances] = train_DMP(az,bz,ax,num_bfs,dt,y_des,timestep);
y_track = rollout(az, bz, ax, tau, dt, weights, centers, variances, g, y0, obstacles, simulation_runtime);

writeObstaclesToFile(obstacles)
writeYtrackToFile(y_track)

if(size(y_des,1) == 1)
    plot(0:dt: (length(y_track)-1) * dt, y_track(1,:),'b--','linewidth', 2);
    hold on
    plot(0:dt:t_stop, y_des(1,:),'r')
    legend('Trained trajectory', 'Input trajectory')
    ylabel('y(t)')
    xlabel('time')
else
    %subplot(3,1,1)
    figure(1)
    plot(y_track(1,:), y_track(2,:),'b--', 'linewidth',2)
    hold on
    plot(y_des(1,:), y_des(2,:), 'r');
    axis([-1 1 -1 1]); axis square;

    xlabel('y1(t)')
    ylabel('y2(t)')
    hold on
    if ~isempty(obstacles)
        plot(obstacles(:,1), obstacles(:,2), 'X', 'MarkerSize', 10)
        legend('Trained trajectory', 'Input trajectory', 'Obstacle')
    end
    figure(2)
    subplot(3,1,1)
    plot(0:dt: (length(y_track)-1) * dt , y_track(1,:),'b--','linewidth', 2);
    hold on
    plot(0:timestep:t_stop, y_des(1,:),'r')
    legend('Trained trajectory dimension 1', 'Input trajectory')
    ylabel('y1(t)')
    xlabel('time')
    
    subplot(3,1,2)
    plot(0:dt: (length(y_track)-1) * dt, y_track(2,:),'b--','linewidth', 2);
    hold on
    plot(0:timestep:t_stop, y_des(2,:),'r')
    legend('Trained trajectory dimension 2', 'Input trajectory')
    ylabel('y2(t)')
    xlabel('time')
    
    subplot(3,1,3)
    plot(0:dt: (length(y_track)-1) * dt, y_track(3,:),'b--','linewidth', 2);
    hold on
    plot(0:timestep:t_stop, y_des(3,:),'r')
    legend('Trained trajectory dimension 3', 'Input trajectory')
    ylabel('y3(t)')
    xlabel('time')
end


